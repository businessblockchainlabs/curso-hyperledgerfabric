package io.fledger.fabric.chaincode.Models;

public class Wallet {
	
	private String walletId;
	private Double tokenAmount;
	
	public Wallet(String walletId, Double tokenAmount) {
		this.walletId=walletId;
		this.tokenAmount=tokenAmount;
	}
	
	public Wallet() {}
	 
	public String getWalletId() {
		return walletId;
	}
	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}
	public Double getTokenAmount() {
		return tokenAmount;
	}
	public void setTokenAmount(Double tokenAmount) {
		this.tokenAmount = tokenAmount;
	}

}
