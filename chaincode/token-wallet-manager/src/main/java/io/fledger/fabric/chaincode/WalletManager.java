package io.fledger.fabric.chaincode;

import java.util.List;

import org.hyperledger.fabric.shim.ChaincodeBase;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.hyperledger.fabric.shim.ResponseUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.fledger.fabric.chaincode.Models.ChaincodeResponse;
import io.fledger.fabric.chaincode.Models.Wallet;

public class WalletManager extends ChaincodeBase {
	
	//ESTE ERA EL ERROR , NOS FALTO INICIAR EL CHAINCODE, POR ELLO NOS DABA EL ERROR
	/*
	 * 
	 * [endorser] SimulateProposal -> ERRO 09c [marketplace][7bc65915] failed to invoke chaincode name:"lscc" , error: container exited with 1
github.com/hyperledger/fabric/core/chaincode.(*RuntimeLauncher).Launch.func1
	/opt/gopath/src/github.com/hyperledger/fabric/core/chaincode/runtime_launcher.go:63
runtime.goexit
	/opt/go/src/runtime/asm_amd64.s:1357
	 */
	
	public static void main (final String [] args) {
		new WalletManager().start(args);
	}

	private String responseError(String errorMessage, String code) {
		try {
			return (new ObjectMapper()).writeValueAsString(new ChaincodeResponse(errorMessage, code, false));
		} catch (Throwable e) {
			return "{\"code\":'" + code + "', \"message\":'" + e.getMessage() + " AND " + errorMessage + "', \"OK\":"
					+ false + "}";
		}

	}

	private String responseSuccess(String successMessage) {
		try {
			return (new ObjectMapper()).writeValueAsString(new ChaincodeResponse(successMessage, "200", true));
		} catch (Throwable e) {
			return "{\"message\":'" + e.getMessage() + " BUT " + successMessage + " (NO COMMIT)', \"OK\":" + false
					+ "}";
		}
	}
	
	private String responseSuccessObject(String object) {
		return "{\"message\":"+object+",\"OK\":"+true+"}";
	}

	private boolean checkString(String str) {
		if (str.trim().length() <= 0 || str == null)
			return false;
		return true;
	}

	@Override
	public Response init(final ChaincodeStub stub) {
		return ResponseUtils.newSuccessResponse(responseSuccess("Init was successfull"));
	}

	@Override
	public Response invoke(final ChaincodeStub stub) {
		String func = stub.getFunction();
		List<String> params = stub.getParameters();

		if (func.equals("createWallet"))
			return createWallet(stub, params);
		else if (func.equals("getWallet"))
			return getWallet(stub, params);
		else if (func.equals("transfer"))
			return transfer(stub, params);

		return ResponseUtils.newErrorResponse(responseError("Unsupported method", "500"));
	}

	private Response createWallet(ChaincodeStub stub, List<String> args) {
		if(args.size()!=2)
			return ResponseUtils.newErrorResponse(responseError("Incorrect number of arguments", "500"));
		String walletId=args.get(0);
		String tokenAmount=args.get(1);
		
		if(!checkString(walletId) || !checkString(tokenAmount))
			return ResponseUtils.newErrorResponse(responseError("Invalid argument(s)", "500"));
		
		double tokenAmountDouble = 0.0;
		try {
			tokenAmountDouble= Double.parseDouble(tokenAmount);
			if(tokenAmountDouble < 0.0)
				return ResponseUtils.newErrorResponse(responseError("Invalid token amount", "500"));
			
		}catch (NumberFormatException e) {
			return ResponseUtils.newErrorResponse (responseError("parseDouble error","500"));
		}
		
		Wallet wallet = new Wallet(walletId, tokenAmountDouble);
		try {
			if(checkString( stub.getStringState(walletId)))
				return ResponseUtils.newErrorResponse(responseError("Existent wallet", "500"));
			stub.putStringState(walletId, (new ObjectMapper()).writeValueAsString(wallet));
			//stub.putState(walletId, (new ObjectMapper()).writeValueAsBytes(wallet));
			return ResponseUtils.newSuccessResponse(responseSuccess("Wallet created"));
			
		}
		catch(Throwable e) {
			return ResponseUtils.newErrorResponse(responseError(e.getMessage(), "500"));
		}	
	}
	
	private Response getWallet(ChaincodeStub stub, List<String> args) {
		if(args.size()!=1)
			return ResponseUtils.newErrorResponse(responseError("Incorrect number of arguments, expecting 1", "500"));
		String walletId=args.get(0);
		
		if(!checkString(walletId))
			return ResponseUtils.newErrorResponse(responseError("Invalid argument(s)", "500"));
		
		try {
			String walletString=stub.getStringState(walletId);
			if(!checkString( walletString))
				return ResponseUtils.newErrorResponse(responseError("Nonexistent wallet", "500"));
			return ResponseUtils.newSuccessResponse((new ObjectMapper()).writeValueAsBytes(responseSuccessObject(walletString)));
		}
		catch(Throwable e) {
			return ResponseUtils.newErrorResponse(responseError(e.getMessage(), "500"));
		}	
	}
	
	private Response transfer(ChaincodeStub stub, List<String> args) {
		if(args.size()!=3)
			return ResponseUtils.newErrorResponse(responseError("Incorrect number of arguments, expecting 3", "500"));
		String fromWalletId=args.get(0);
		String toWalletId=args.get(1);
		String tokenAmount=args.get(2);
		
		if(!checkString(fromWalletId) || !checkString(toWalletId) ||!checkString(tokenAmount))
			return ResponseUtils.newErrorResponse(responseError("Invalid argument(s)", "500"));
		if(fromWalletId.equals(toWalletId))
			return ResponseUtils.newErrorResponse(responseError("From-wallet is same as to-wallet", "500"));
		
		double tokenAmountDouble = 0.0;
		try {
			tokenAmountDouble= Double.parseDouble(tokenAmount);
			if(tokenAmountDouble < 0.0)
				return ResponseUtils.newErrorResponse(responseError("Invalid token amount", "500"));
			
		}catch (NumberFormatException e) {
			return ResponseUtils.newErrorResponse (responseError("parseDouble error","500"));
		}
		
		try {
			String fromWalletString= stub.getStringState(fromWalletId);
			if(!checkString(fromWalletString))
				 return ResponseUtils.newErrorResponse(responseError("Nonexistent from-wallet", ""));
			String toWalletString= stub.getStringState(toWalletId);
			if(!checkString(toWalletString))
				 return ResponseUtils.newErrorResponse(responseError("Nonexistent to-wallet", ""));
			
			ObjectMapper objectMappper =new ObjectMapper();
			Wallet fromWallet = objectMappper.readValue(fromWalletString,Wallet.class);
			Wallet toWallet = objectMappper.readValue(toWalletString,Wallet.class);
			
			if(fromWallet.getTokenAmount()< tokenAmountDouble)
				 return ResponseUtils.newErrorResponse(responseError("Token amount not enough", ""));
			
			fromWallet.setTokenAmount(fromWallet.getTokenAmount()-tokenAmountDouble);
			toWallet.setTokenAmount(toWallet.getTokenAmount()+tokenAmountDouble);
			stub.putStringState(fromWalletId, objectMappper.writeValueAsString(fromWallet));
			stub.putStringState(toWalletId, objectMappper.writeValueAsString(toWallet));
			return ResponseUtils.newSuccessResponse(responseSuccess("Transfered"));
			
		}
		catch(Throwable e) {
			return ResponseUtils.newErrorResponse(responseError(e.getMessage(), "500"));
		}	
	}
}
