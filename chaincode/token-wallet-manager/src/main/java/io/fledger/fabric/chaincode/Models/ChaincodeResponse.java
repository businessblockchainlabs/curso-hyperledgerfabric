package io.fledger.fabric.chaincode.Models;

public class ChaincodeResponse {
	
	private String message;
	private String code;
	private boolean OK;
	
	public ChaincodeResponse(String message, String code, boolean OK) {
		this.setMessage(message);
		this.setCode(code);
		this.setOK(OK);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isOK() {
		return OK;
	}

	public void setOK(boolean oK) {
		OK = oK;
	}
	

}
